package com.sample;

import java.io.*;
import java.text.MessageFormat;
import java.util.List;

/**
 * Reads input file with character grid and words and outputs locations of
 * the words in the character character grid to the console.
 * 1. Words are printed in the order specified.
 * 2. Word search is case sensitive.
 * 3. Only matching words are printed.
 * 4. Words that match multiple times are printed in undefined order
 * (implementation specific).
 */
public class AlphabetSoup {
    /**
     * @param args single input file name parameter
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: alphabet-soup inputFile");
        } else {
            try {
                // get character grid and words to search for

                FileSoupProblemProvider soupProblemProvider = new FileSoupProblemProvider();

                //alex    SoupProblem soupProblem = soupProblemProvider.getSoupProblem("c:\\work\\input3.txt");
                SoupProblem soupProblem = soupProblemProvider.getSoupProblem(args[0]);

                // find words in character grid

                SoupProblemSolver soupProblemSolver = new BasicSoupProblemSolver(soupProblem.getSoupGrid());

                List<SoupResult> soupResults = soupProblemSolver.findWords(soupProblem.getWords());

                // write words found in character grid along with their positions to standard output

                for (SoupResult soupResult : soupResults) {
                    System.out.println(MessageFormat.format("{0} {1}:{2} {3}:{4}", soupResult.getWord(), soupResult.getY(), soupResult.getX(), soupResult.getY2(), soupResult.getX2()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
