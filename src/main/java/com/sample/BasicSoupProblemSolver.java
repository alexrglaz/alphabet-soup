package com.sample;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Basic alphabet soup problem solver that finds specified words in a character grid.
 * Words may appear in the grid horizonally, vertically, or diagonally, in either
 * direction. It works by first creating a list of horizonal, vertical, and
 * diagonal grid fragments that span the whole grid. For each fragment, it stores the
 * fragment, starting position, and horizontal and vertical increments. To find words
 * in a specified list, it traverses the whole list for each word, looking for fragments
 * that contain that word.  The search is case-sensitive.
 */
public class BasicSoupProblemSolver implements SoupProblemSolver {
    private SoupGrid soupGrid;
    private SoupGridMatrix soupGridMatrix;

    /**
     * @param soupGrid
     */
    public BasicSoupProblemSolver(SoupGrid soupGrid) {
        this.soupGrid = soupGrid;
    }

    /**
     * @return grid.
     */
    protected SoupGrid getSoupGrid() {
        return soupGrid;
    }

    /**
     * Create soup grid matrix.
     */
    protected SoupGridMatrix createSoupGridMatrix() {
        SoupGridMatrix soupGridMatrix = new SoupGridMatrix();

        StringBuilder sb = new StringBuilder();
        char[][] grid = soupGrid.getGrid();
        int rows = soupGrid.getRowSize();
        int columns = soupGrid.getColumnSize();

        // add horizonal fragments

        for (int row = 0; row < rows; ++row) {
            String line = String.valueOf(grid[row]);

            soupGridMatrix.addLineToGridMatrix(line, 0, row, 1, 0);
        }

        // add vertical fragments

        for (int col = 0; col < columns; ++col) {
            for (int row = 0; row < rows; ++row) {
                sb.append(grid[row][col]);
            }

            soupGridMatrix.addLineToGridMatrix(sb.toString(), col, 0, 0, 1);
            sb.setLength(0);
        }

        // add diagonal left to right fragments

        for (int outerColumn = 0; outerColumn < columns; ++outerColumn) {
            for (int column = outerColumn, row = 0; column < columns && row < rows; ++column, ++row) {
                sb.append(grid[row][column]);
            }

            soupGridMatrix.addLineToGridMatrix(sb.toString(), outerColumn, 0, 1, 1);
            sb.setLength(0);
        }

        for (int outerRow = 1; outerRow < rows; ++outerRow) {
            for (int column = 0, row = outerRow; column < columns && row < rows; ++column, ++row) {
                sb.append(grid[row][column]);
            }

            soupGridMatrix.addLineToGridMatrix(sb.toString(), 0, outerRow, 1, 1);
            sb.setLength(0);
        }

        // add diagonal right to left fragments

        for (int outerColumn = columns - 1; outerColumn >= 0; --outerColumn) {
            for (int column = outerColumn, row = 0; column >= 0 && row < rows; --column, ++row) {
                sb.append(grid[row][column]);
            }

            soupGridMatrix.addLineToGridMatrix(sb.toString(), outerColumn, 0, -1, 1);
            sb.setLength(0);
        }

        for (int outerRow = 1; outerRow < rows; ++outerRow) {
            for (int column = columns - 1, row = outerRow; column >= 0 && row < rows; --column, ++row) {
                sb.append(grid[row][column]);
            }

            soupGridMatrix.addLineToGridMatrix(sb.toString(), columns - 1, outerRow, -1, 1);
            sb.setLength(0);
        }

        return soupGridMatrix;
    }

    /**
     * Find words in grid.
     *
     * @param words to file
     * @return list of words along with positions in grid.
     */
    public List<SoupResult> findWords(List<String> words) {
        List<SoupResult> soupResults = new ArrayList<>();

        if (soupGridMatrix == null) {
            soupGridMatrix = createSoupGridMatrix();
        }
        // look for each word in list of horizonal, vertical, and diagonal grid fragments

        for (String word : words) {
            String searchableWord = word.replace(" ", "");
            Map<String, Collection<SoupGridLine>> gridMatrix = soupGridMatrix.getGridMatrix().asMap();

            for (Map.Entry<String, Collection<SoupGridLine>> gridLineEntry : gridMatrix.entrySet()) {
                int indexOf = gridLineEntry.getKey().indexOf(searchableWord);

                // if word found in grid, add result along with starting and ending positions

                if (indexOf != -1) {

                    Collection<SoupGridLine> soupGridLines = gridLineEntry.getValue();

                    for (SoupGridLine soupGridLine : soupGridLines) {
                        int wordLength = searchableWord.length();
                        int xIncrement = soupGridLine.getXIncrement();
                        int yIncrement = soupGridLine.getYIncrement();
                        int x = soupGridLine.getX() + indexOf * xIncrement;
                        int y = soupGridLine.getY() + indexOf * yIncrement;
                        int x2 = x + wordLength * xIncrement - xIncrement;
                        int y2 = y + wordLength * yIncrement - yIncrement;

                        SoupResult soupResult = new SoupResult(word, x, y, x2, y2);

                        soupResults.add(soupResult);
                    }
                }
            }
        }

        return soupResults;
    }
}