package com.sample;

import org.apache.commons.collections4.MultiValuedMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Expanded alphabet soup problem solver that finds specified words in a
 * character grid. Words may appear in the grid horizonally, vertically, or
 * diagonally, in either direction.  It works by first creating a list of all
 * possible horizonal, vertical, and diagonal words in the grid. For each word,
 * it stores the fragment, starting position, and horizontal and vertical
 * increments in a multimap. To find words, it uses a dictionary lookup for each
 * word in the specified list.  The lookup is case-sensitive.  This solver is
 * faster than BasicSoupProblemSolver but it uses more memory.
 */
public class ExpandedSoupProblemSolver extends BasicSoupProblemSolver {
    private SoupGridMatrix soupGridMatrix;

    /**
     * @param soupGrid
     */
    public ExpandedSoupProblemSolver(SoupGrid soupGrid) {
        super(soupGrid);
    }

    /**
     * Create grid matrix.
     */
    @Override
    protected SoupGridMatrix createSoupGridMatrix() {
        SoupGridMatrix soupGridMatrix = new SoupGridMatrix();

        // get list of all horizonal, vertical, and diagonal fragments that span the whole character grid

        SoupGridMatrix basicSoupGridMatrix = super.createSoupGridMatrix();
        Map<String, Collection<SoupGridLine>> basicGridMatrix = basicSoupGridMatrix.getGridMatrix().asMap();

        // add all possible words from grid

        for (Map.Entry<String, Collection<SoupGridLine>> gridLineEntry : basicGridMatrix.entrySet()) {
            String line = gridLineEntry.getKey();

            int lineLength = line.length();

            for (int i = 0; i < lineLength; ++i) {
                for (int j = i + 1; j <= lineLength; ++j) {
                    String subLine = line.substring(i, j);

                    for (SoupGridLine soupGridLine : gridLineEntry.getValue()) {
                        int xIncrement = soupGridLine.getXIncrement();
                        int yIncrement = soupGridLine.getYIncrement();
                        int x = soupGridLine.getX() + xIncrement * i;
                        int y = soupGridLine.getY() + yIncrement * i;

                        soupGridMatrix.addLineToGridMatrix(subLine, x, y, xIncrement, yIncrement, false);
                    }
                }
            }
        }

        return soupGridMatrix;
    }

    /**
     * Find words in grid.
     *
     * @param words to file
     * @return list of words along with positions in grid.
     */
    public List<SoupResult> findWords(List<String> words) {
        List<SoupResult> soupResults = new ArrayList<>();

        if (soupGridMatrix == null) {
            soupGridMatrix = createSoupGridMatrix();
        }

        MultiValuedMap<String, SoupGridLine> gridMatrix = soupGridMatrix.getGridMatrix();

        // look for each word in list of horizonal, vertical, and diagonal words from grid

        for (String word : words) {
            String searchableWord = word.replace(" ", "");
            Collection<SoupGridLine> soupGridLines = gridMatrix.get(searchableWord);

            // for each word found in grid, add result along with starting and ending positions

            for (SoupGridLine soupGridLine : soupGridLines) {
                int wordLength = searchableWord.length();
                int x = soupGridLine.getX();
                int y = soupGridLine.getY();
                int xIncrement = soupGridLine.getXIncrement();
                int yIncrement = soupGridLine.getYIncrement();
                int x2 = x + wordLength * xIncrement - xIncrement;
                int y2 = y + wordLength * yIncrement - yIncrement;

                SoupResult soupResult = new SoupResult(word, x, y, x2, y2);

                soupResults.add(soupResult);
            }
        }

        return soupResults;
    }
}
