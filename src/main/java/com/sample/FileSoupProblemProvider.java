package com.sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads soup problems by reading them from a file.
 *
 * Format:
 * [rows]x[columns]
 * grid (spaces are ignored)
 * words to search for
 *
 * Format sample:
 * 2x3
 * A B C
 * D E F
 * ABC
 * AEI
 */
public class FileSoupProblemProvider {
    public FileSoupProblemProvider() {
    }

    /**
     * Loads soup problem from file.
     *
     * @param fileName name of input file
     * @return soup problem
     * @throws IOException
     */
    public SoupProblem getSoupProblem(String fileName) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            int rowSize = 0;
            int columnSize = 0;

            // get number of rows and columns for grid

            if ((line = br.readLine()) != null) {
                String tokens[] = line.split("x");

                rowSize = Integer.parseInt(tokens[0]);
                columnSize = Integer.parseInt(tokens[1]);
            }

            // get grid

            char[][] grid = new char[rowSize][columnSize];

            for (int i = 0; i < rowSize; ++i) {
                line = br.readLine().replace(" ", "");

                grid[i] = line.toCharArray();
            }

            // get list of words to search for

            List<String> words = new ArrayList<>();

            while ((line = br.readLine()) != null) {
                words.add(line);
            }

            return new SoupProblem(new SoupGrid(grid, rowSize, columnSize), words);
        }
    }
}
