package com.sample;

/**
 * Grid of characters
 */
public class SoupGrid {
    private int rowSize;
    private int columnSize;
    private char[][] grid;

    /**
     * @param grid
     * @param rowSize
     * @param columnSize
     */
    SoupGrid(char[][] grid, int rowSize, int columnSize) {
        this.grid = grid;
        this.rowSize = rowSize;
        this.columnSize = columnSize;
    }

    /**
     * @return number of rows.
     */
    public int getRowSize() {
        return rowSize;
    }

    /**
     * @return number of columns.
     */
    public int getColumnSize() {
        return columnSize;
    }

    /**
     * @return grid of characters.
     */
    public char[][] getGrid() {
        return grid;
    }
}