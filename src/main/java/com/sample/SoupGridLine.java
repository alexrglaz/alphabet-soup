package com.sample;

/**
 * line in grid with starting position and direction increments.
 */
public class SoupGridLine {
    private String line;
    private int x;
    private int y;
    private int xIncrement;
    private int yIncrement;

    /**
     * @param line
     * @param x
     * @param y
     * @param xIncrement
     * @param yIncrement
     */
    SoupGridLine(String line, int x, int y, int xIncrement, int yIncrement) {
        this.line = line;
        this.x = x;
        this.y = y;
        this.xIncrement = xIncrement;
        this.yIncrement = yIncrement;
    }

    /**
     * @return line
     */
    public String getLine() {
        return line;
    }

    /**
     * @return starting x coorginate of line in grid.
     */
    public int getX() {
        return x;
    }

    /**
     * @return starting y coorginate of line in grid.
     */
    public int getY() {
        return y;
    }

    /**
     * @return x increment of corresponding to direction of line.
     */
    public int getXIncrement() {
        return xIncrement;
    }

    /**
     * @return y increment of corresponding to direction of line.
     */
    public int getYIncrement() {
        return yIncrement;
    }
}