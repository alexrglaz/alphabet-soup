package com.sample;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.StringUtils;

/**
 * matrix of word fragments
 */
public class SoupGridMatrix {
    private MultiValuedMap<String, SoupGridLine> gridMatrix;

    public SoupGridMatrix() {
        gridMatrix = new ArrayListValuedHashMap<>();
    }

    /**
     * Add line to grid matrix along with optional reverse of line.
     *
     * @param line line fragment
     * @param x starting horizontal position
     * @param y starting vertical position
     * @param xIncrement horizontal increment
     * @param yIncrement vertical increment
     * @param addReverse whether to add line in reverse
     */
    public void addLineToGridMatrix(String line, int x, int y, int xIncrement, int yIncrement, boolean addReverse) {
        gridMatrix.put(line, new SoupGridLine(line, x, y, xIncrement, yIncrement));

        if (addReverse) {
            int lineLength = line.length();

            if (lineLength > 1) {
                line = StringUtils.reverse(line);
                x += lineLength * xIncrement - xIncrement;
                y += lineLength * yIncrement - yIncrement;
                xIncrement = -xIncrement;
                yIncrement = -yIncrement;

                gridMatrix.put(line, new SoupGridLine(line, x, y, xIncrement, yIncrement));
            }
        }
    }

    /**
     * Add line to grid matrix.
     *
     * @param line line fragment
     * @param x starting horizontal position
     * @param y starting vertical position
     * @param xIncrement horizontal increment
     * @param yIncrement vertical increment
     */
    public void addLineToGridMatrix(String line, int x, int y, int xIncrement, int yIncrement) {
        addLineToGridMatrix(line, x, y, xIncrement, yIncrement, true);
    }

    /**
     * @return grid matrix.
     */
    public MultiValuedMap<String, SoupGridLine> getGridMatrix() {
        return gridMatrix;
    }
}
