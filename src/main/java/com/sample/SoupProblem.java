package com.sample;

import java.util.List;

/**
 * Character grid and words to search.
 */
public class SoupProblem {
    private SoupGrid soupGrid;
    private List<String> words;

    /**
     * @param soupGrid
     * @param words
     */
    SoupProblem(SoupGrid soupGrid, List<String> words) {
        this.soupGrid = soupGrid;
        this.words = words;
    }

    /**
     * @return character grid.
     */
    public SoupGrid getSoupGrid() {
        return soupGrid;
    }

    /**
     * @return words to search.
     */
    public List<String> getWords() {
        return words;
    }
}
