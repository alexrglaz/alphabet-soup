package com.sample;

import java.util.List;

public interface SoupProblemSolver {
    List<SoupResult> findWords(List<String> words);
}
