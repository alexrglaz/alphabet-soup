package com.sample;

/**
 * Word along with position found in a grid.
 */
public class SoupResult {
    private String word;
    private int x;
    private int y;
    private int x2;
    private int y2;

    SoupResult(String word, int x, int y, int x2, int y2) {
        this.word = word;
        this.x = x;
        this.y = y;
        this.x2 = x2;
        this.y2 = y2;
    }

    /**
     * @return word.
     */
    public String getWord() {
        return this.word;
    }

    /**
     * @return x coordinate for start of word.
     */
    public int getX() {
        return x;
    }

    /**
     * @return y coordinate for start of word.
     */
    public int getY() {
        return y;
    }

    /**
     * @return x coordinate for end of word.
     */
    public int getX2() {
        return x2;
    }

    /**
     * @return y coordinate for end of word.
     */
    public int getY2() {
        return y2;
    }
}
