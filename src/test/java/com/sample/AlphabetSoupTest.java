package com.sample;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class AlphabetSoupTest {
    private PrintStream originalStdout;
    private ByteArrayOutputStream consoleText;
    private String inputFileName;
    private String outputText;

    @org.junit.jupiter.api.Test
    void main() {
        // invoke main

        AlphabetSoup.main(new String[] { inputFileName });

        // compare output with output file

        assertEquals(consoleText.toString(), outputText);
    }

    @BeforeEach
    void setUp() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();

        // save original stdout

        originalStdout = System.out;

        // get input file name

        inputFileName = classLoader.getResource("sampleinput.txt").getFile();

        // get output file text

        File outputFile = new File(classLoader.getResource("sampleoutput.txt").getFile());

        outputText = FileUtils.readFileToString(outputFile, "UTF-8");

        // set stdout to stream

        consoleText = new ByteArrayOutputStream();

        System.setOut(new PrintStream(consoleText));
    }

    @AfterEach
    void tearDown() {
        // restore original stdout

        System.setOut(originalStdout);
    }
}